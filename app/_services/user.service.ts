﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    server:string="http://35.188.148.146";

    getAll() {
        return this.http.get<User[]>(this.server+'/api/user');
    }

    getById(id: number) {
        return this.http.get(this.server+'/api/user/' + id);
    }

    create(user: User) {

        return this.http.post(this.server+'/api/user', user);
    }

    update(user: User) {
        return this.http.put(this.server+'/api/user/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(this.server+'/api/user/' + id);
    }
}
